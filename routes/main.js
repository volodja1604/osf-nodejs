const router=require('express').Router();
const Category=require('../models/category');
const Product=require('../models/product');

 router.get('/',(req,res)=>{
    res.render('main/home');
 });
 //category route
 router.get('/:id',(req,res,next)=>{
	Category.find({id:req.params.id},function(err,category){
		if(err) return next(err);
		res.render('main/category',{
			category:category,
			breadcrumb:[{title:'Home',url:'/'},
						{title:category[0].name, url:req.url}
						]
		});
	});

});
 //subcategory route
 router.get('/:categoryName/:category_id',(req,res,next)=>{
	Product.find({primary_category_id:req.params.category_id},function(err,products){
		if(err) return next(err);
		res.render('main/products',{
			products:products,
			breadcrumb:[{title:'Home',url:'/'},
			{title:req.params.categoryName, url:`/${req.params.categoryName}`},
            {title:req.params.category_id, url:req.url}
			],
			mainCategory:req.params.categoryName
		});
	});


});
//single product route
router.get('/:mainCategory/:category_id/:id',(req,res,next)=>{
	Product.find({_id:req.params.id},function(err,product){
		if(err) return next(err);
		res.render('main/product',{
		product:product[0],
		breadcrumb:[{title:'Home',url:'/'},
			{title:req.params.mainCategory, url:`/${req.params.mainCategory}`},
			{title:req.params.category_id, url:`/${req.params.mainCategory}/${req.params.category_id}`},
			{title:product[0].name, url:req.url},
		]
		});
	})

});


 module.exports=router;
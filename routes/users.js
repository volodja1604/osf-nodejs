const router=require('express').Router();
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User=require('../models/user');
const passportConfig=require('../config/passport');
router.get('/register',(req,res)=>{
    res.render('main/register');
 });
 router.get('/login',(req,res)=>{
    if(req.user){
		return res.redirect('/');
	}
	res.render('main/login',{message:req.flash('loginMessage')});
 });
 router.post('/register', (req,res)=>{
    const user=new User();
    user.name=req.body.name;
    user.email=req.body.email;
    user.password=req.body.password;
    User.findOne({email:req.body.email},function(err,existedUser){
      if(existedUser){
          req.flash('errors', 'Account with that email already exists');
          return res.redirect('/register');
      }else{
          user.save(function(err,user){
              if(err) return next(err);
              req.logIn(user,function(err){
                  if(err) return next(err);
                  res.redirect('/');
              });
          });
      }
    });
 });
  // Login Processing
  router.post('/login', (req, res, next) => {
    passport.authenticate('local-login', {
      successRedirect:'/',
      failureRedirect:'/users/login',
      failureFlash: true
    })(req, res, next);
  });
  
  
  router.get('/logout',(req,res,next)=>{
      req.logout();
      req.flash('success_msg', ' you are logout');
      res.redirect('/');
  });
  //access contol
  function ensureAthenticated(req,res,next){
      if (req.isAuthenticated()){
          return next()
      }
      else{
          res.redirect('/login');
      }
  }
  
 module.exports=router;
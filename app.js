const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const path=require('path');
const cookieParser=require('cookie-parser');
const expressValidator=require('express-validator');
const flash=require('express-flash');
const session=require('express-session');
const passport=require('passport');
const LocalStrategy=require('passport-local').Strategy;
const MongoStore=require('connect-mongo')(session);
const engine = require('ejs-mate');

const secret = require('./config/secret');
const Category = require('./models/category');
const Product = require('./models/product');
const appRoutes=require('./routes/main');
const usersRoutes=require('./routes/users');
const app = express();

mongoose.connect(secret.database, function (err) {
    if (err) { console.log(err); }
    else {
        console.log('Connected to database')
    }
});
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: 'secret',
    store:new MongoStore({url:secret.database,autoReconnect:true})
  }));


app.use(passport.initialize());
app.use(passport.session());
app.use(expressValidator({
    errorFormatter:function(param, msg, value){
        const namespace=param.split('.')
        ,root=namespace.shift()
        , formParam=root;
        while(namespace.length){
            formParam+='['+namespace.shift()+']';

        }
        return {
            param:formParam,
            msg:msg,
            value:value
        };
    }
}));
app.use(flash());
app.use(function (req, res, next) {
    res.locals.success_msg=req.flash('success_msg');
    res.locals.error_msg=req.flash('error_msg');
    res.locals.error=req.flash('error');
    res.locals.user=req.user;
    next();
});
app.use(function (req, res, next) {
    Category.find({}, function (err, categories) {
        if (err) return next(err);
        res.locals.categories = categories;
        res.locals.title = 'Shop';
        next();
    })
});
app.use(function (req, res, next) {
    Product.find({}, function (err, products) {
        if (err) return next(err);
        res.locals.products = products;

        next();
    })
});
app.engine('ejs', engine);
app.set('view engine', 'ejs');

app.use('/users', usersRoutes);
app.use(appRoutes);

app.listen(secret.port,function(err){
	if(err){
		throw err;
	}
	console.log(`server is running on port ${secret.port}`);
});
const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var ProductSchema=new Schema({
	primary_category_id:{type:String},
	image_groups:[
		{
			images:[
				{
					alt:String,
					link:String,
					title:String
				}
			],
			view_type:String
		}
	],
	price:Number,
	name:{type:String},
	image:String,
	page_description:String,
	page_title:String,
	currency:String,
	orderable:Boolean,
	c_isSale:Boolean,
	long_description:String,
	short_description:String,
	id:String

});

module.exports=mongoose.model('Product',ProductSchema);
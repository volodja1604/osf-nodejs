const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var CategorySchema=new Schema({
	name:{type:String},
	page_description:String,
	categories:[
		{categories:[
			{id:String,
			page_description:String,
			page_title:String,
			image:String,
			name:String}
			],
		name:String
		}
	]

});
module.exports=mongoose.model('Category',CategorySchema);